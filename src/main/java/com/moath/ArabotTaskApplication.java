package com.moath;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan
public class ArabotTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArabotTaskApplication.class, args);
	}

}
