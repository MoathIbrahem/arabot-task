package com.moath.controller;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.moath.entity.ApplicationUser;
import com.moath.security.JWTSecurityConstants;
import com.moath.security.JWTUtility;
import com.moath.security.UserSecurityDetails;
import com.moath.security.UserSecurityDetailsService;
import com.moath.service.ApplicationUserService;

@RestController
@CrossOrigin
public class ApplicationUserController {
	@Autowired
	private ApplicationUserService userService;

	@Autowired
	private UserSecurityDetailsService userSecurityDetailsService;

	@Autowired
	private JWTUtility jwtUtility;

	@GetMapping("/users/{userId}")
	public Optional<ApplicationUser> getUser(@PathVariable Long userId) {
		return userService.getApplicationUser(userId);
	}

	@PutMapping("/users/{userId}")
	public void updtaeApplicationUser(@RequestBody ApplicationUser user) {
		userService.updateApplicationUser(user);
	}

	@PostMapping("/users")
	public void addApplicationUser(@RequestBody ApplicationUser user) {
		userService.addApplicationUser(user);
	}

	@PutMapping("/users/{application_user_id}/language")
	public void updateUserAccount(HttpServletResponse httpServletResponse,
			@PathVariable("application_user_id") Long userId, @RequestBody String languageId) throws IOException {
		userService.updateUserLanguage(userId, languageId);
	}

	@GetMapping("/token/refresh")
	public void refreshToken(HttpServletResponse httpServletResponse) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = (String) auth.getPrincipal();
		UserSecurityDetails userSecurityDetails = (UserSecurityDetails) userSecurityDetailsService
				.loadUserByUsername(username);
		String token = jwtUtility.generateJWT(userSecurityDetails);
		httpServletResponse.addHeader(JWTSecurityConstants.HEADER_KEY, JWTSecurityConstants.TOKEN_PREFIX + token);
		httpServletResponse.addHeader("Access-Control-Expose-Headers", JWTSecurityConstants.HEADER_KEY);
	}
}
