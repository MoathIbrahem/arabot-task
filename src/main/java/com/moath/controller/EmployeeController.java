package com.moath.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.moath.entity.Employee;
import com.moath.exception.ServiceException;
import com.moath.service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@PostMapping("/employees")
	public int addEmployee(@RequestBody Employee employee) throws ServiceException {
		return employeeService.addEmployee(employee);
	}

	@DeleteMapping("/employees/{employeeId}")
	public void deleteEmployee(@PathVariable int employeeId) throws ServiceException {
		employeeService.deleteEmployee(employeeId);
	}

	@PutMapping("/employees/{employeeId}")
	public void updateEmployee(@RequestBody Employee employee) throws ServiceException {
		employeeService.updateEmployee(employee);
	}

	@GetMapping("/employees")
	public List<Employee> getEmployeesByfilters(@RequestParam Map<String, String> filters) throws ServiceException {
		return employeeService.searchEmployee(filters);
	}

	@PutMapping("/employees/{employeeId}/raise/{ratio}")
	public Employee raiseEmployeeSalary(@RequestBody Employee employee, @PathVariable double ratio)
			throws ServiceException {
		return employeeService.raiseEmployeeSalary(employee, ratio);
	}

	@GetMapping("/employees/top-paid")
	public List<Employee> getTopPaidEmployees() {
		return employeeService.getTopPaidEmployees();
	}

	@GetMapping("/employees/salary/{type}")
	public double getSalaryByEquationType(@PathVariable String type) {

		if (type.equals("avgSalary")) {
			return employeeService.getAvgSalary();
		} else if (type.equals("median")) {
			return employeeService.getMedianSalary();
		} else if (type.equals("standardDeviation")) {
			return employeeService.getStandardDeviationSalary();
		}
		return 1.5;
	}
}
