package com.moath.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.moath.entity.CVInfo;
import com.moath.service.CVInfoService;

@RestController
public class CVInfoController {
	@Autowired
	private CVInfoService cvInfoService;

	@PostMapping("/employees/{employeeId}/cv")
	public void uploadEmployeeCV(@PathParam("employeeId") int employeeId, @RequestParam("cvFile") MultipartFile file)
			throws IOException {
		cvInfoService.uploadEmployeeCV(employeeId, file);
	}

	@GetMapping("/cvs")
	public List<CVInfo> getCVsContainsKeyWord(@RequestParam Map<String, String> filters) {
		return cvInfoService.searchCV(filters);
	}
}
