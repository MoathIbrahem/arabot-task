package com.moath.security;

import java.io.IOException;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private AuthenticationManager authenticationManager;

	private JWTUtility jwtUtility;

	public JWTAuthenticationFilter(AuthenticationManager authenticationManager, JWTUtility jwtUtility) {
		this.authenticationManager = authenticationManager;
		this.jwtUtility = jwtUtility;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {

		try {
			Map<String, String> userAuthentication = new ObjectMapper().readValue(request.getInputStream(),
					new TypeReference<Map<String, String>>() {
					});
			UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
					userAuthentication.get("username"), userAuthentication.get("password"));
			return authenticationManager.authenticate(authenticationToken);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authentication) throws IOException, ServletException {

		UserSecurityDetails userSecurityDetails = (UserSecurityDetails) authentication.getPrincipal();
		String token = jwtUtility.generateJWT(userSecurityDetails);

		response.addHeader(JWTSecurityConstants.HEADER_KEY, JWTSecurityConstants.TOKEN_PREFIX + token);
		response.addHeader("Access-Control-Expose-Headers", JWTSecurityConstants.HEADER_KEY);
	}

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authenticationException) throws IOException, ServletException {

		SecurityContextHolder.clearContext();
		response.setStatus(HttpStatus.UNAUTHORIZED.value());
		if (authenticationException instanceof BadCredentialsException) {
			response.getWriter().print("INVALID_LOGIN");
		} else if (authenticationException instanceof DisabledException) {
			response.getWriter().print("USER_DISABLED");
		}
	}

}
