package com.moath.security;

import java.util.Arrays;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.moath.entity.UserRole;

public class UserSecurityDetails extends User {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String name;
	private UserRole role;

	public UserSecurityDetails(String username, String password, UserRole role) {
		super(username, password, true, true, true, true, Arrays.asList(new SimpleGrantedAuthority(role.name())));
		this.role = role;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UserRole getRole() {
		return role;
	}

}