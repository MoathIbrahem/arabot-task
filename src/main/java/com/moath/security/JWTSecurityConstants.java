package com.moath.security;

public class JWTSecurityConstants {

	public static final long ACCESS_TOKEN_VALIDITY = 7 * 24 * 60 * 60 * 1000;
	public static final String USER_ROLE_KEY = "role";
	public static final String SIGNING_KEY = "arabot256secret2018plus";
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_KEY = "Authorization";
	public static final String ISSUER = "http://arabot-task.com";
}
