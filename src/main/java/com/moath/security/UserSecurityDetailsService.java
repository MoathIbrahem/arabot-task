package com.moath.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.moath.entity.ApplicationUser;
import com.moath.repository.ApplicationUserRepository;

@Service
public class UserSecurityDetailsService implements UserDetailsService {

	@Autowired
	private ApplicationUserRepository repository;

	@Transactional
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		ApplicationUser applicationUser = repository.findByEmailAddress(username);
		if (applicationUser == null) {
			throw new UsernameNotFoundException("User " + username + " not found");
		}
		UserSecurityDetails userSecurityDetails = new UserSecurityDetails(applicationUser.getFullName(),
				applicationUser.getPassword(), applicationUser.getUserRole());
		userSecurityDetails.setId(applicationUser.getId());
		userSecurityDetails.setName(applicationUser.getFirstName() + " " + applicationUser.getLastName());
		return userSecurityDetails;
	}

}
