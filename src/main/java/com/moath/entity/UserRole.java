package com.moath.entity;

public enum UserRole {
	ROLE_ADMIN_USER, ROLE_APPLICATION_USER;

	public static final String ADMIN_USER_ROLE_EXPRESSION = "hasRole('ADMIN_USER')";
	public static final String APPLICATION_USER_ROLE_EXPRESSION = "hasRole('APPLICATION_USER')";

}
