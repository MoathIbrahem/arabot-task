package com.moath.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class CVInfo {
	private int id;
	private String cvData;
	private String cvPath;
	private Employee employee;

	public CVInfo() {
	}

	public CVInfo(int id, String cvData, String cvPath, Employee employee) {
		this.id = id;
		this.cvData = cvData;
		this.cvPath = cvPath;
		this.employee = employee;
	}

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "cv_data", length = 10000, nullable = false)
	public String getCvData() {
		return cvData;
	}

	public void setCvData(String cvData) {
		this.cvData = cvData;
	}

	@OneToOne
	@JoinColumn(name = "employee_id")
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@Column(name = "cv_path", length = 300, nullable = false)
	public String getCvPath() {
		return cvPath;
	}

	public void setCvPath(String cvPath) {
		this.cvPath = cvPath;
	}

}
