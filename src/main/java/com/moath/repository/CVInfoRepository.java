package com.moath.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.moath.entity.CVInfo;

public interface CVInfoRepository extends JpaRepository<CVInfo, Integer>, JpaSpecificationExecutor<CVInfo> {

	public Optional<CVInfo> findById(Integer cvId);
}
