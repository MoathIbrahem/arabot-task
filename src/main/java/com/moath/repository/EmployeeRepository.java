package com.moath.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.moath.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer>, JpaSpecificationExecutor<Employee> {

	public Optional<Employee> findById(Integer employeeId);

	public List<Employee> findByName(String employeeName);

	public List<Employee> findTop3ByOrderBySalaryDesc();

	public Employee findTopByOrderBySalaryAsc();

	@Query("SELECT AVG(emp.salary) FROM Employee emp")
	public double findAvgBySalary();

	@Query("SELECT salary FROM Employee order by salary asc")
	public List<Double> findAllBySalary();

}
