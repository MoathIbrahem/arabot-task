package com.moath.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.moath.entity.ApplicationUser;

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {

	public ApplicationUser findByEmailAddress(String emailAddress);

}
