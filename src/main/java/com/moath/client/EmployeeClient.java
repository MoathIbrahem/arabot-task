package com.moath.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moath.entity.Department;
import com.moath.entity.Employee;

public class EmployeeClient {
    private static String jwtTokenKey = "Authorization";
    private static String jwtTokenValue = "";

    public static void main(String[] args) {
        Employee employee = new Employee(700, "Employee1", 500, true,
                new Department(1, "dept 1", new ArrayList<Employee>(0)));
        Map<String, String> tokenInfo = getJWTToken();
        jwtTokenValue = tokenInfo.get(jwtTokenKey);
        System.out.println("--------------------------------------------ADD EMPOYEE");
        int employeeIdToBeDeleted = addEmployeeClient(employee);
        System.out.println("--------------------------------------------RAISE EMPOYEE SALARY");
        raisEmployeeSalaryClient(employee, 1.5);
        System.out.println("--------------------------------------------UPDATE EMPOYEE");
        updateEmployeeClient(employee, 20);
        System.out.println("--------------------------------------------DELETE EMPOYEE");
        deleteEmployeeClient(employeeIdToBeDeleted);
        System.out.println("--------------------------------------------TOP PAID EMPOYEES");
        getTopPaidEmployees();
        System.out.println("--------------------------------------------SALARY WITH EQUATION");
        getSalaryByEquationType("standardDeviation");
        System.out.println("--------------------------------------------SEARCH EMPLOYEES WITH FILTERS");
        Map<String, String> filters = new HashMap<String, String>();
        filters.put("employeeName", "Employee1");
        getEmployeesByfilters(filters);
    }

    public static Map<String, String> getJWTToken() {

        String addEmployee = "http://localhost:8080/token/auth";
        URL addEmployeeURL;
        try {
            addEmployeeURL = new URL(addEmployee);
            HttpURLConnection conn = (HttpURLConnection) addEmployeeURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            String userJson = "{ \"username\" : \"admin@admin.com\", \"password\" : \"123\" }";

            OutputStream os = conn.getOutputStream();
            os.write(userJson.getBytes());
            os.flush();
            os.close();

            int responseCode = conn.getResponseCode();

            System.out.println(conn.getHeaderField("Authorization"));

            System.out.println("POST Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                Map<String, String> authData = new HashMap<>();
                authData.put("Authorization", conn.getHeaderField("Authorization"));
                return authData;
            } else {
                System.out.println("POST request not worked");
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void raisEmployeeSalaryClient(Employee employee, double ratio) {
        String raisEmployeeSalary = "http://localhost:8080/employees/" + employee.getId() + "/raise/" + ratio;
        URL raisEmployeeSalaryURL;
        try {
            raisEmployeeSalaryURL = new URL(raisEmployeeSalary);
            HttpURLConnection conn = (HttpURLConnection) raisEmployeeSalaryURL.openConnection();
            conn.setRequestMethod("PUT");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty(jwtTokenKey, jwtTokenValue);
            conn.setDoOutput(true);
            ObjectMapper mapper = new ObjectMapper();
            String employeeJson = mapper.writeValueAsString(employee);
            OutputStream os = conn.getOutputStream();
            os.write(employeeJson.getBytes());
            os.flush();
            os.close();
            int responseCode = conn.getResponseCode();
            System.out.println("PUT Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                System.out.println(response.toString());
            } else {
                System.out.println("POST request not worked");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static int addEmployeeClient(Employee employee) {
        String addEmployee = "http://localhost:8080/employees/";
        URL addEmployeeURL;
        try {
            addEmployeeURL = new URL(addEmployee);
            HttpURLConnection conn = (HttpURLConnection) addEmployeeURL.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty(jwtTokenKey, jwtTokenValue);
            conn.setDoOutput(true);
            ObjectMapper mapper = new ObjectMapper();
            String employeeJson = mapper.writeValueAsString(employee);
            OutputStream os = conn.getOutputStream();
            os.write(employeeJson.getBytes());
            os.flush();
            os.close();
            int responseCode = conn.getResponseCode();
            System.out.println("POST Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                return Integer.valueOf(response.toString());
            } else {
                System.out.println("POST request not worked");
                return -1;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static void deleteEmployeeClient(int employeeId) {
        String addEmployee = "http://localhost:8080/employees/" + employeeId;
        URL addEmployeeURL;
        try {
            addEmployeeURL = new URL(addEmployee);
            HttpURLConnection conn = (HttpURLConnection) addEmployeeURL.openConnection();
            conn.setRequestMethod("DELETE");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty(jwtTokenKey, jwtTokenValue);
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();
            System.out.println("DELETE Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                System.out.println(response.toString());
            } else {
                System.out.println("DELETE request not worked");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void updateEmployeeClient(Employee employee, int employeeId) {
        String addEmployee = "http://localhost:8080/employees/" + employeeId;
        URL addEmployeeURL;
        try {
            addEmployeeURL = new URL(addEmployee);
            HttpURLConnection conn = (HttpURLConnection) addEmployeeURL.openConnection();
            conn.setRequestMethod("PUT");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty(jwtTokenKey, jwtTokenValue);
            conn.setDoOutput(true);
            ObjectMapper mapper = new ObjectMapper();
            String employeeJson = mapper.writeValueAsString(employee);
            OutputStream os = conn.getOutputStream();
            os.write(employeeJson.getBytes());
            os.flush();
            os.close();
            int responseCode = conn.getResponseCode();
            System.out.println("PUT Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                System.out.println(response.toString());
            } else {
                System.out.println("PUT request not worked");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Employee> getTopPaidEmployees() {
        String addEmployee = "http://localhost:8080/employees/top-paid";
        URL addEmployeeURL;
        try {
            addEmployeeURL = new URL(addEmployee);
            HttpURLConnection conn = (HttpURLConnection) addEmployeeURL.openConnection();
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty(jwtTokenKey, jwtTokenValue);
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            int responseCode = conn.getResponseCode();
            System.out.println("GET Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                ObjectMapper mapper = new ObjectMapper();
                List<Employee> employees = Arrays.asList(mapper.readValue(response.toString(), Employee[].class));
                System.out.println(response.toString());
                return employees;
            } else {
                System.out.println("GET request not worked");
            }
            conn.getInputStream();
            conn.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static double getSalaryByEquationType(String type) {
        String addEmployee = "http://localhost:8080/employees/salary/" + type;
        URL addEmployeeURL;
        try {
            addEmployeeURL = new URL(addEmployee);
            HttpURLConnection conn = (HttpURLConnection) addEmployeeURL.openConnection();
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty(jwtTokenKey, jwtTokenValue);
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            int responseCode = conn.getResponseCode();
            System.out.println("GET Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                return Double.valueOf(response.toString());
            } else {
                System.out.println("GET " + type + " request failed");
            }
            conn.getInputStream();
            conn.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0.0;
    }

    public static List<Employee> getEmployeesByfilters(Map<String, String> filters) {

        StringBuilder addEmployee = new StringBuilder("http://localhost:8080/employees");
        if (!filters.isEmpty()) {
            addEmployee.append("?");
            for (Map.Entry<String, String> entry : filters.entrySet()) {
                addEmployee.append(entry.getKey() + "=" + entry.getValue());
            }
        }
        URL addEmployeeURL;
        try {
            addEmployeeURL = new URL(addEmployee.toString());
            HttpURLConnection conn = (HttpURLConnection) addEmployeeURL.openConnection();
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty(jwtTokenKey, jwtTokenValue);
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            int responseCode = conn.getResponseCode();
            System.out.println("GET Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                ObjectMapper mapper = new ObjectMapper();
                List<Employee> employees = Arrays.asList(mapper.readValue(response.toString(), Employee[].class));
                System.out.println(response.toString());
                return employees;
            } else {
                System.out.println("GET Employees with filters request failed");
            }
            conn.getInputStream();
            conn.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
