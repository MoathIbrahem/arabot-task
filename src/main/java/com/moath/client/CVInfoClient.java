package com.moath.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moath.entity.CVInfo;
import com.moath.utility.MultipartUtility;

public class CVInfoClient {

	public static int uploadEmployeeCV(String filePath, int employeeId) {
		String charset = "UTF-8";
		File uploadFile1 = new File(filePath);
		String requestURL = "http://localhost:8080/employees/cv";

		try {
			MultipartUtility multipart = new MultipartUtility(requestURL, charset);
			multipart.addFormField("employeeId", Integer.toString(employeeId));
			multipart.addFilePart("cvFile", uploadFile1);

			return multipart.finish();
		} catch (IOException ex) {
			System.err.println(ex);
		}
		return -1;
	}

	public static List<CVInfo> getCVsContainsKeyWord(Map<String, String> filters) {

		StringBuilder addEmployee = new StringBuilder("http://localhost:8080/cvs");
		if (!filters.isEmpty()) {
			addEmployee.append("?");
			try {
				for (Map.Entry<String, String> entry : filters.entrySet()) {
					addEmployee.append(entry.getKey() + "=" + URLEncoder.encode(entry.getValue(), "UTF-8"));
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		URL addEmployeeURL;
		try {
			addEmployeeURL = new URL(addEmployee.toString());
			HttpURLConnection conn = (HttpURLConnection) addEmployeeURL.openConnection();
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			int responseCode = conn.getResponseCode();
			System.out.println("GET Response Code :: " + responseCode);
			if (responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				ObjectMapper mapper = new ObjectMapper();
				List<CVInfo> cvs = Arrays.asList(mapper.readValue(response.toString(), CVInfo[].class));
				System.out.println(response.toString());
				return cvs;
			} else {
				System.out.println("GET Employees with filters request failed");
			}
			conn.getInputStream();
			conn.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		System.out.println("---------------------------------------");
		Map<String, String> filters = new HashMap<String, String>();
		filters.put("keyword", "Software Engineer");
		getCVsContainsKeyWord(filters);
		System.out.println("---------------------------------------");
	}

}
