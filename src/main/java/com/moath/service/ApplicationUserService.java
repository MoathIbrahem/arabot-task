package com.moath.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.moath.entity.ApplicationUser;
import com.moath.repository.ApplicationUserRepository;

@Service
public class ApplicationUserService {
	@Autowired
	ApplicationUserRepository applicationUserRepository;

	@Autowired
	private BCryptPasswordEncoder encoder;

	public void addApplicationUser(ApplicationUser applicationUser) {
		applicationUser.setPassword(encoder.encode(applicationUser.getPassword()));
		applicationUserRepository.save(applicationUser);
	}

	public void deleteApplicationUser(Long userId) {
		applicationUserRepository.deleteById(userId);
	}

	public void updateApplicationUser(ApplicationUser applicationUser) {
		applicationUser.setPassword(encoder.encode(applicationUser.getPassword()));
		applicationUserRepository.save(applicationUser);
	}

	public Optional<ApplicationUser> getApplicationUser(Long applicationUserId) {
		return applicationUserRepository.findById(applicationUserId);

	}

	public void updateUserLanguage(Long userId, String languageId) {
		ApplicationUser user = getApplicationUser(userId).get();
		applicationUserRepository.save(user);
	}

}
