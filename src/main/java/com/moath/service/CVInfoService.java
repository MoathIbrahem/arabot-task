package com.moath.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.moath.entity.CVInfo;
import com.moath.entity.Department;
import com.moath.entity.Employee;
import com.moath.repository.CVInfoRepository;
import com.moath.repository.DepartmentRepository;
import com.moath.repository.EmployeeRepository;

@Service
public class CVInfoService {

	@Autowired
	CVInfoRepository cvInfoRepository;

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	DepartmentRepository departmentRepository;

	@Autowired
	private Environment env;

	public void uploadEmployeeCV(int employeeId, MultipartFile file) {

		Date date = new Date();

		Department dept = new Department();
		dept.setName("IIT");
		departmentRepository.save(dept);
		Employee employee = new Employee();
		employee.setActive(true);
		employee.setName("Moath");
		employee.setSalary(1200);
		employee.setDepartment(dept);
		employeeRepository.save(employee);
		long timeMilli = date.getTime();
		Path cvsFolderPath = Paths.get(env.getProperty("employee_cvs_folder_path"));
		try {
			String cvPath = env.getProperty("employee_cvs_folder_path") + "\\" + timeMilli + file.getOriginalFilename();
			Files.copy(file.getInputStream(), cvsFolderPath.resolve(cvPath));
			CVInfo cvInfo = new CVInfo();
			Employee emp = employeeRepository.findById(employeeId).get();
			cvInfo.setCvData(extractCVData(cvPath));
			cvInfo.setCvPath(cvPath);
			cvInfo.setEmployee(emp);
			cvInfoRepository.saveAndFlush(cvInfo).getId();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected String extractCVData(String cvPath) {

		String cvParsedData = "";
		try {
			PDDocument document = PDDocument.load(new File(cvPath));
			document.getClass();
			if (!document.isEncrypted()) {
				PDFTextStripperByArea stripper = new PDFTextStripperByArea();
				stripper.setSortByPosition(true);
				PDFTextStripper Tstripper = new PDFTextStripper();
				String str = Tstripper.getText(document);
				Scanner scn = null;
				scn = new Scanner(str);
				while (scn.hasNextLine()) {
					cvParsedData += scn.nextLine();
				}
				scn.close();
			}
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return cvParsedData;
	}

	public List<CVInfo> searchCV(Map<String, String> filters) {

		Map<String, String> conditions = new HashMap<>();
		if (filters != null && !filters.isEmpty()) {
			filters.entrySet().forEach(entry -> {
				conditions.put(String.valueOf(entry.getKey()), entry.getValue());
			});
		}
		return findCVs(conditions);
	}

	public final List<CVInfo> findCVs(Map<String, String> filters) {

		Specification<CVInfo> specification = new Specification<CVInfo>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<CVInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				query.distinct(true);
				Predicate predicate = criteriaBuilder
						.and(getConditionsPredicates(root, criteriaBuilder, filters).toArray(new Predicate[0]));
				return predicate;
			}
		};
		return cvInfoRepository.findAll(specification);
	}

	protected List<Predicate> getConditionsPredicates(Root<CVInfo> root, CriteriaBuilder criteriaBuilder,
			Map<String, String> filters) {

		List<Predicate> conditions = new ArrayList<>();
		if (filters == null || filters.isEmpty()) {
			return conditions;
		}
		for (Entry<String, String> filterEntry : filters.entrySet()) {
			if (filterEntry.getValue() == null || filterEntry.getValue().isEmpty()) {
				continue;
			}
			if (filterEntry.getKey().equals("keyword")) {
				conditions.add(criteriaBuilder.like(root.get("cvData"), "%" + filterEntry.getValue() + "%"));
			}
		}
		return conditions;
	}

}
