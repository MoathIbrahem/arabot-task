package com.moath.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.moath.entity.Employee;
import com.moath.exception.ServiceException;
import com.moath.repository.CVInfoRepository;
import com.moath.repository.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	CVInfoRepository cvInfoRepository;

	public int addEmployee(Employee employee) throws ServiceException {
		if (employee.getName() == null) {
			throw new ServiceException("Employee Name is NULL");
		}
		employeeRepository.saveAndFlush(employee);
		return employee.getId();
	}

	public void deleteEmployee(int employeeId) throws ServiceException {
		if (employeeId <= 0) {
			throw new ServiceException("Invalid Employee ID");
		}
		employeeRepository.deleteById(employeeId);
	}

	public void updateEmployee(Employee employee) throws ServiceException {
		if (employee.getName() == null) {
			throw new ServiceException("Invalid Employee Name");
		}
		employeeRepository.saveAndFlush(employee);
	}

	public List<Employee> getEmployeeByName(String name) throws ServiceException {
		if (name == null) {
			throw new ServiceException("Invalid Employee Name");
		}
		return employeeRepository.findByName(name);
	}

	public List<Employee> searchEmployee(Map<String, String> filters) throws ServiceException {
		if (filters == null || filters.isEmpty()) {
			throw new ServiceException("Invalid Filters");
		}
		Map<String, String> conditions = new HashMap<>();
		if (filters != null && !filters.isEmpty()) {
			filters.entrySet().forEach(entry -> {
				conditions.put(String.valueOf(entry.getKey()), entry.getValue());
			});
		}
		return findEmployees(conditions);
	}

	public final List<Employee> findEmployees(Map<String, String> filters) {
		return employeeRepository.findAll(getSpecification(filters));
	}

	public Specification<Employee> getSpecification(Map<String, String> filters) {
		Specification<Employee> specification = new Specification<Employee>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<Employee> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				query.distinct(true);
				Predicate predicate = criteriaBuilder
						.and(getConditionsPredicates(root, criteriaBuilder, filters).toArray(new Predicate[0]));
				return predicate;
			}
		};
		return specification;

	}

	 List<Predicate> getConditionsPredicates(Root<Employee> root, CriteriaBuilder criteriaBuilder,
			Map<String, String> filters) {

		List<Predicate> conditions = new ArrayList<>();
		if (filters == null || filters.isEmpty()) {
			return conditions;
		}
		for (Entry<String, String> filterEntry : filters.entrySet()) {
			if (filterEntry.getValue() == null || filterEntry.getValue().isEmpty()) {
				continue;
			}
			if (filterEntry.getKey().equals("employeeName")) {
				conditions.add(criteriaBuilder.like(root.get("name"), "%" + filterEntry.getValue() + "%"));
			} else if (filterEntry.getKey().equals("departmentId")) {
				conditions.add(criteriaBuilder.equal(root.join("department").get("id"),
						Long.parseLong(filterEntry.getValue())));
			} else if (filterEntry.getKey().equals("employeeStatus")) {
				conditions.add(criteriaBuilder.equal(root.get("active"), Boolean.parseBoolean(filterEntry.getValue())));
			}
		}
		return conditions;
	}

	public Employee raiseEmployeeSalary(Employee employee, double ratio) throws ServiceException {
		if (employee.getName() == null) {
			throw new ServiceException("Invalid Employee Name");
		} else if (ratio <= 0) {
			throw new ServiceException("Invalid Ratio Value");
		}
		employee.setSalary(employee.getSalary() * ratio);
		employeeRepository.saveAndFlush(employee);
		return employee;

	}

	public List<Employee> getTopPaidEmployees() {
		return employeeRepository.findTop3ByOrderBySalaryDesc();
	}

	public double getAvgSalary() {
		return employeeRepository.findAvgBySalary();
	}

	public double getMedianSalary() {

		List<Double> salaries = employeeRepository.findAllBySalary();
		if (salaries.size() % 2 == 0) {
			return salaries.get(salaries.size() / 2);
		} else {
			double medianRight = salaries.get(salaries.size() / 2);
			double medianLeft = salaries.get(salaries.size() / 2 - 1);
			return (medianRight + medianLeft) / 2;
		}
	}

	public double getStandardDeviationSalary() {
		return standardDeviation(employeeRepository.findAllBySalary());
	}

	protected static double standardDeviation(List<Double> numbers) {

		double sum = 0.0, standardDeviation = 0.0;
		int length = numbers.size();

		for (double num : numbers) {
			sum += num;
		}

		double mean = sum / length;

		for (double num : numbers) {
			standardDeviation += Math.pow(num - mean, 2);
		}

		return Math.sqrt(standardDeviation / length);
	}
}
