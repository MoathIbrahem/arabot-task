package com.moath.arabottask;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.moath.entity.Department;
import com.moath.entity.Employee;
import com.moath.exception.ServiceException;
import com.moath.repository.EmployeeRepository;
import com.moath.service.EmployeeService;

@SpringBootTest
public class EmployeeServiceMockTest {

	@Autowired
	private EmployeeService employeeService = mock(EmployeeService.class);

	@MockBean
	private EmployeeRepository employeeRepository;

	@Test
	public void addEmployeeTest() throws ServiceException {
		Employee employee = new Employee(1, "moath", 1233, true,
				new Department(1, "dept 1", new ArrayList<Employee>(0)));
		when(employeeRepository.save(employee)).thenReturn(employee);
		assertEquals(employee.getId(), employeeService.addEmployee(employee));
	}

	@Test
	public void deleteEmployeeTest_WithIdLessThanZero() throws ServiceException {
		Employee employee = new Employee(1, "moath", 1233, true,
				new Department(1, "dept 1", new ArrayList<Employee>(0)));
		assertThrows(ServiceException.class, () -> {
			employeeService.deleteEmployee(employee.getId());
		});
	}

	@Test
	public void updateEmployeeTest() {
		Employee employee = new Employee(1, "Employee1", 1233, true,
				new Department(1, "dept 1", new ArrayList<Employee>(0)));
		when(employeeRepository.save(employee)).thenReturn(employee);
		employee.setName("Employee2");
		when(employeeRepository.save(employee)).thenReturn(employee);
		assertNotEquals("Employee1", employee.getName());
	}

	@Test
	public void getTopPaidEmployeesTest() {
		when(employeeService.getTopPaidEmployees()).thenReturn(Stream.of(
				new Employee(1, "Employee1", 1233, true, new Department(1, "dept1", new ArrayList<Employee>(0))),
				new Employee(2, "Employee2", 1232, true, new Department(1, "dept2", new ArrayList<Employee>(0))),
				new Employee(3, "Employee3", 1231, true, new Department(1, "dept3", new ArrayList<Employee>(0))))
				.collect(Collectors.toList()));
		assertEquals(3, employeeService.getTopPaidEmployees().size());
	}

	@Test
	public void getEmployeeByNameTest_WithEmployeeName() throws ServiceException {
		String name = "Employee1";
		when(employeeRepository.findByName(name)).thenReturn(Stream
				.of(new Employee(1, "Employee1", 1233, true, new Department(1, "dept1", new ArrayList<Employee>(0))))
				.collect(Collectors.toList()));
		assertEquals(1, employeeService.getEmployeeByName(name).size());
	}

	@Test
	public void getEmployeeByNameTest_WithNullEmployeeName() throws ServiceException {
		String name = null;
		assertThrows(ServiceException.class, () -> {
			employeeService.getEmployeeByName(name);
		});
	}

	@Test
	public void searchEmployeeTest_WithEmptyFilters() {

		Map<String, String> filters = new HashMap<String, String>();
		assertThrows(ServiceException.class, () -> {
			employeeService.searchEmployee(filters);
		});
	}

	@Test
	public void searchEmployeeTest_WithNullFilters() {

		Map<String, String> filters = null;
		assertThrows(ServiceException.class, () -> {
			employeeService.searchEmployee(filters);
		});
	}

}
