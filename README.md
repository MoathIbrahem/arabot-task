# Prerequisites:
- Maven
- GIT
- JDK 8
- MySql 8


# Steps to install and run the applicaiton on local machine:
You need to create a schema in the database called arabot and the MySQL service port should be : 3306
- Clone the app with the following command :
```
git clone https://MoathIbrahem@bitbucket.org/MoathIbrahem/arabot-task.git```
- Build the project with the following command :
```
mvn clean install
```
- Start the application with the following commmand :
```
mvn spring-boot:run
```
- Run client code
You can find EmployeeClient class that contain main method :
You need execute this query on the database : 
insert into application_user values (1, "admin", "admin", "123", "$2a$10$8iLdUGL3fbnRa.LTnitvluQaDTFJCdD/BN7SXicicjq8cjjR9SDTy", "admin@admin.com");
Before calling any method on this class you need to call getJWTToken method to generate jwt token.
To add an employee call addEmployeeClient method and pass an object.
To get top paid employees call getTopPaidEmployees method.



